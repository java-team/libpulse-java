#!/bin/sh

VERSION=$2
TAR=../libpulse-java_$VERSION.orig.tar.xz


tar -xf $3
rm $3

cd icedtea-$VERSION
XZ_OPT=--best tar -cJvf ../$TAR pulseaudio
cd ..

rm -Rf icedtea-$VERSION
